# Goals

This repository contains spack recipies for compiling Shaman and other
tools.

# How to use this repository

## Configuring spack
### General setup
First, install a spack instance by cloning spack: https://github.com/spack/spack.git

```
$ git clone https://github.com/spack/spack.git
$ cd spack
$ . share/spack/setup-env.sh
```

We encourage you to build a new /recent/ c++ compiler:
```
$ spack install gcc@7.3.0 +binutils
```

and then register it as a usable compiler in spack:
```
$ spack compiler add $(spack find -p gcc@7.3.0)
```

### Using this repository
Add this repository to the spack repo list:
in etc/spack/defaults/repos.yaml add the *shaman_spack_repo_path*
```YAML
repos:
  - *shaman_spack_repo_path*
  - $spack/var/spack/repos/builtin
```

# Installing software
```
$ spack install shamanizer
```

