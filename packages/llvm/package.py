##############################################################################
# Copyright (c) 2013-2018, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/spack/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Llvm(CMakePackage):
    """The LLVM Project is a collection of modular and reusable compiler and
       toolchain technologies. Despite its name, LLVM has little to do
       with traditional virtual machines, though it does provide helpful
       libraries that can be used to build them. The name "LLVM" itself
       is not an acronym; it is the full name of the project.
    """

    homepage = 'http://llvm.org/'
    url = 'http://llvm.org/releases/3.7.1/llvm-3.7.1.src.tar.xz'
    list_url = 'http://releases.llvm.org/download.html'

    family = 'compiler'  # Used by lmod

    # currently required by mesa package
    version('3.0', 'a8e5f5f1c1adebae7b4a654c376a6005',
            url='http://llvm.org/releases/3.0/llvm-3.0.tar.gz')

    # NOTE: The debug version of LLVM is an order of magnitude larger than
    # the release version, and may take up 20-30 GB of space. If you want
    # to save space, build with `build_type=Release`.

    variant('clang', default=True,
            description="Build the LLVM C/C++/Objective-C compiler frontend")
    variant('lldb', default=True, description="Build the LLVM debugger")
    variant('lld', default=True, description="Build the LLVM linker")
    variant('internal_unwind', default=True,
            description="Build the libcxxabi libunwind")
    variant('polly', default=True,
            description="Build the LLVM polyhedral optimization plugin, "
            "only builds for 3.7.0+")
    variant('libcxx', default=True,
            description="Build the LLVM C++ standard library")
    variant('compiler-rt', default=True,
            description="Build LLVM compiler runtime, including sanitizers")
    variant('gold', default=True,
            description="Add support for LTO with the gold linker plugin")
    variant('shared_libs', default=False,
            description="Build all components as shared libraries, faster, "
            "less memory to build, less stable")
    variant('link_dylib', default=False,
            description="Build and link the libLLVM shared library rather "
            "than static")
    variant('all_targets', default=True,
            description="Build all supported targets, default targets "
            "<current arch>,NVPTX,AMDGPU,CppBackend")
    variant('build_type', default='Release',
            description='CMake build type',
            values=('Debug', 'Release', 'RelWithDebInfo', 'MinSizeRel'))
    variant('python', default=False, description="Install python bindings")
    extends('python', when='+python')

    # Build dependency
    depends_on('cmake@3.4.3:', type='build')

    # Universal dependency
    depends_on('python@2.7:2.8', when='@:4.999')
    depends_on('python')
    depends_on('py-lit', type=('build', 'run'))

    # lldb dependencies
    depends_on('ncurses', when='+lldb')
    depends_on('swig', when='+lldb')
    depends_on('libedit', when='+lldb')
    depends_on('py-six', when='@5.0.0: +lldb +python')

    # gold support
    depends_on('binutils+gold', when='+gold')

    # polly plugin
    depends_on('gmp', when='@:3.6.999 +polly')
    depends_on('isl', when='@:3.6.999 +polly')

    base_url = 'http://prereleases.llvm.org/%%(baseversion)s/%%(rc)s/%(pkg)s-%%(version)s.src.tar.xz'
    llvm_url = base_url % {'pkg': 'llvm'}

    resources = {
        'compiler-rt': {
            'url': base_url % {'pkg': 'compiler-rt'},
            'destination': 'projects',
            'placement': 'compiler-rt',
            'variant': '+compiler-rt',
        },
        'openmp': {
            'url': base_url % {'pkg': 'openmp'},
            'destination': 'projects',
            'placement': 'openmp',
            'variant': '+clang',
        },
        'libcxx': {
            'url': base_url % {'pkg': 'libcxx'},
            'destination': 'projects',
            'placement': 'libcxx',
            'variant': '+libcxx',
        },
        'libcxxabi': {
            'url':  base_url % {'pkg': 'libcxxabi'},
            'destination': 'projects',
            'placement': 'libcxxabi',
            'variant': '+libcxx',
        },
        'cfe': {
            'url':  base_url % {'pkg': 'cfe'},
            'destination': 'tools',
            'placement': 'clang',
            'variant': '+clang',
        },
        'clang-tools-extra': {
            'url':  base_url % {'pkg': 'clang-tools-extra'},
            'destination': 'tools/clang/tools',
            'placement': 'extra',
            'variant': '+clang',
        },
        'lldb': {
            'url':  base_url % {'pkg': 'lldb'},
            'destination': 'tools',
            'placement': 'lldb',
            'variant': '+lldb',
        },
        'lld': {
            'url':  base_url % {'pkg': 'lld'},
            'destination': 'tools',
            'placement': 'lld',
            'variant': '+lld',
        },
        'polly': {
            'url':  base_url % {'pkg': 'polly'},
            'destination': 'tools',
            'placement': 'polly',
            'variant': '+polly',
        },
        'libunwind': {
            'url':  base_url % {'pkg': 'libunwind'},
            'destination': 'projects',
            'placement': 'libunwind',
            'variant': '+internal_unwind',
        },
    }
    releases = [
        {
            'version': '7.0.0rc3',
            'baseversion': '7.0.0',
            'rc': 'rc3',
            'md5': '2bcf6ed0e8f7e3385a5c8b28c0343b8b',
            'resources': {
                'compiler-rt': '5ce0adf3df4a393da6decb9f43d556db',
                'openmp': 'a2d526e990652b13a3e40806a37c8c8c',
                'polly': 'bd3660b35b72d97a96fdf450dabccaec',
                'libcxx': '89e7803ce7c777e1510c02aa779117de',
                'libcxxabi': '31f88936f1678e56341b89a1612d9a4f',
                'cfe': 'a00ba4eac92c1259c505758913db00c4',
                'clang-tools-extra': '6b247ca70aafe1bc883f14dbd17f9676',
                'lldb': 'a7e1243c472a7a938c75d838ace2d33b',
                'lld': 'dc727dff37b43d8482448a8388888eec',
                'libunwind': 'e12bed46bcb4d8f1756b62c70d517599'
            }
        },
        {
            'version': '7.0.0rc2',
            'baseversion': '7.0.0',
            'rc': 'rc2',
            'md5': 'd59efe2d71f14105760f2f5603fff25d',
            'resources': {
                'compiler-rt': 'bb7a487d7198b8ecd45d0f426536ec4f',
                'openmp': '3c85397adab075ca42c4f472e78b517a',
                'polly': '091ce799afdfd0c006cbaf953dac1454',
                'libcxx': 'c3d19456d37769219559d25ca52238fd',
                'libcxxabi': 'a0faf446acd10144409053c4b0650f90',
                'cfe': 'a0da5b5d09eb40b8f28649a7e98df7ee',
                'clang-tools-extra': '15297a3635498b3099bc5806780ab1c2',
                'lldb': '3994cb2441a914d0b3f88b7b4521398f',
                'lld': 'ae48f23983a25b3e16fc9f0a6e9b3085',
                'libunwind': '9be4638cf75d30e99a1d8c7dffe519fc',
            }
        },
        {
            'version': '7.0.0rc1',
            'baseversion': '7.0.0',
            'rc': 'rc1',
            'md5': 'e8c0d13ffa6acdc644613d92d74ad18b',
            'resources': {
                'compiler-rt': '182c46bc734c5d68b32cbde2858011b8',
                'openmp': '32f44edb54f42fc3604faf0273739741',
                'polly': 'fc362e63bea76ea1246058870035f6ca',
                'libcxx': 'd4323fa789e3434fce66a32f77882f09',
                'libcxxabi': 'bc48004b27ac1b9a62a6cae542390284',
                'cfe': 'cd31bd6a99b2ed367856077efe817df4',
                'clang-tools-extra': '9f86e0f9ac75db2341e29bff1f495529',
                'lldb': 'bda7975bdda7524398779104b84508b9',
                'lld': '72d7c04e36ce44c6f1f7f128c40ca876',
                'libunwind': '30fb2d79c997c8d21a673605040c0df6',
            }
        },
    ]

    for release in releases:
        version(release['version'], release['md5'], url=llvm_url % release)

        for name, md5 in release['resources'].items():
            resource(name=name,
                     url=resources[name]['url'] % release,
                     md5=md5,
                     destination=resources[name]['destination'],
                     when='@%s%s' % (release['version'],
                                     resources[name].get('variant', "")),
                     placement=resources[name].get('placement', None))

    conflicts('+clang_extra', when='~clang')
    conflicts('+lldb',        when='~clang')

    # Github issue #4986
    patch('llvm_gcc7.patch', when='@4.0.0:4.0.1+lldb %gcc@7.0:')

    @run_before('cmake')
    def check_darwin_lldb_codesign_requirement(self):
        if not self.spec.satisfies('+lldb platform=darwin'):
            return
        codesign = which('codesign')
        cp = which('cp')
        mkdir('tmp')
        llvm_check_file = join_path('tmp', 'llvm_check')
        cp('/usr/bin/false', llvm_check_file)

        try:
            codesign('-f', '-s', 'lldb_codesign', '--dryrun',
                     llvm_check_file)

        except ProcessError:
            explanation = ('The "lldb_codesign" identity must be available'
                           ' to build LLVM with LLDB. See https://llvm.org/'
                           'svn/llvm-project/lldb/trunk/docs/code-signing'
                           '.txt for details on how to create this identity.')
            raise RuntimeError(explanation)

    def setup_environment(self, spack_env, run_env):
        spack_env.append_flags('CXXFLAGS', self.compiler.cxx11_flag)

        if '+clang' in self.spec:
            run_env.set('CC', join_path(self.spec.prefix.bin, 'clang'))
            run_env.set('CXX', join_path(self.spec.prefix.bin, 'clang++'))

    def cmake_args(self):
        spec = self.spec

        cmake_args = [
            '-DLLVM_REQUIRES_RTTI:BOOL=ON',
            '-DCLANG_DEFAULT_OPENMP_RUNTIME:STRING=libomp',
            '-DPYTHON_EXECUTABLE:PATH={0}'.format(spec['python'].command.path),
        ]

        if '+gold' in spec:
            cmake_args.append('-DLLVM_BINUTILS_INCDIR=' +
                              spec['binutils'].prefix.include)
        if '+polly' in spec:
            cmake_args.append('-DLINK_POLLY_INTO_TOOLS:Bool=ON')
        else:
            cmake_args.extend(['-DLLVM_EXTERNAL_POLLY_BUILD:Bool=OFF',
                               '-DLLVM_TOOL_POLLY_BUILD:Bool=OFF',
                               '-DLLVM_POLLY_BUILD:Bool=OFF',
                               '-DLLVM_POLLY_LINK_INTO_TOOLS:Bool=OFF'])

        if '+python' in spec and '+lldb' in spec and spec.satisfies('@5.0.0:'):
            cmake_args.append('-DLLDB_USE_SYSTEM_SIX:Bool=TRUE')
        if '+clang' not in spec:
            cmake_args.append('-DLLVM_EXTERNAL_CLANG_BUILD:Bool=OFF')
        if '+lldb' not in spec:
            cmake_args.extend(['-DLLVM_EXTERNAL_LLDB_BUILD:Bool=OFF',
                               '-DLLVM_TOOL_LLDB_BUILD:Bool=OFF'])
        if '+lld' not in spec:
            cmake_args.append('-DLLVM_TOOL_LLD_BUILD:Bool=OFF')
        if '+internal_unwind' not in spec:
            cmake_args.append('-DLLVM_EXTERNAL_LIBUNWIND_BUILD:Bool=OFF')
        if '+libcxx' in spec:
            if spec.satisfies('@3.9.0:'):
                cmake_args.append('-DCLANG_DEFAULT_CXX_STDLIB=libc++')
        else:
            cmake_args.append('-DLLVM_EXTERNAL_LIBCXX_BUILD:Bool=OFF')
            cmake_args.append('-DLLVM_EXTERNAL_LIBCXXABI_BUILD:Bool=OFF')
        if '+compiler-rt' not in spec:
            cmake_args.append('-DLLVM_EXTERNAL_COMPILER_RT_BUILD:Bool=OFF')

        if '+shared_libs' in spec:
            cmake_args.append('-DBUILD_SHARED_LIBS:Bool=ON')

        if '+link_dylib' in spec:
            cmake_args.append('-DLLVM_LINK_LLVM_DYLIB:Bool=ON')

        if '+all_targets' not in spec:  # all is default on cmake

            if spec.version < Version('3.9.0'):
                targets = ['CppBackend', 'NVPTX', 'AMDGPU']
            else:
                # Starting in 3.9.0 CppBackend is no longer a target (see
                # LLVM_ALL_TARGETS in llvm's top-level CMakeLists.txt for
                # the complete list of targets)
                targets = ['NVPTX', 'AMDGPU']

            if 'x86' in spec.architecture.target.lower():
                targets.append('X86')
            elif 'arm' in spec.architecture.target.lower():
                targets.append('ARM')
            elif 'aarch64' in spec.architecture.target.lower():
                targets.append('AArch64')
            elif 'sparc' in spec.architecture.target.lower():
                targets.append('Sparc')
            elif ('ppc' in spec.architecture.target.lower() or
                  'power' in spec.architecture.target.lower()):
                targets.append('PowerPC')

            cmake_args.append(
                '-DLLVM_TARGETS_TO_BUILD:Bool=' + ';'.join(targets))

        if spec.satisfies('@4.0.0:') and spec.satisfies('platform=linux'):
            cmake_args.append('-DCMAKE_BUILD_WITH_INSTALL_RPATH=1')
        return cmake_args

    @run_before('build')
    def pre_install(self):
        with working_dir(self.build_directory):
            # When building shared libraries these need to be installed first
            make('install-LLVMTableGen')
            if self.spec.version >= Version('4.0.0'):
                # LLVMDemangle target was added in 4.0.0
                make('install-LLVMDemangle')
            make('install-LLVMSupport')

    @run_after('install')
    def post_install(self):
        if '+clang' in self.spec and '+python' in self.spec:
            install_tree(
                'tools/clang/bindings/python/clang',
                join_path(site_packages_dir, 'clang'))

        with working_dir(self.build_directory):
            install_tree('bin', join_path(self.prefix, 'libexec', 'llvm'))
