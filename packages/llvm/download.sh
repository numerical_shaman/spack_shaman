#! /bin/bash

resources="llvm  compiler-rt openmp polly libcxx libcxxabi cfe clang-tools-extra lldb lld libunwind"

base_url="http://prereleases.llvm.org/"
version="7.0.0"
rc="rc3"

for i in ${resources}; do
  echo "resource = $i";
  wget -c "${base_url}/${version}/${rc}/${i}-${version}${rc}.src.tar.xz";
done

for i in ${resources}; do
  md=$(md5sum ${i}-${version}${rc}.src.tar.xz | cut -f1 -d' ') ;
  echo "'${i}': '${md}', " ;
done
